/*
       // *****************************************************
       // * Copyright (c) 2016, IBM Corporation
       // * All rights reserved.
       // *
       // * Redistribution and use in source and binary forms,
       // * with or without modification, are permitted provided
       // * that the following conditions are met:
       // * - Redistributions of source code must retain
       // *   the above copyright notice, this list of conditions
       // *   and the following disclaimer.
       // * - Redistributions in binary form must reproduce the
       // *   above copyright notice, this list of conditions
       // *   and the following disclaimer in the documentation
       // *   and/or other materials provided with the distribution.
       // * - Neither the name of the IBM Corporation nor the names
       // *   of its contributors may be used to endorse or promote
       // *   products derived from this software without specific
       // *   prior written permission.
       // *
       // * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
       // * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
       // * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
       // * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
       // * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
       // * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
       // * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
       // * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
       // * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
       // * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
       // * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
       // * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
       // * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
       // * POSSIBILITY OF SUCH DAMAGE.
       // *****************************************************

  $Id$
*/


#define	PHP_ILE_VERSION "1.0.1"


#ifndef PHP_ILE_H
#define PHP_ILE_H


#ifdef ZTS
#include "TSRM.h"
#endif


PHP_MINIT_FUNCTION(ile);
PHP_RINIT_FUNCTION(ile);
PHP_MSHUTDOWN_FUNCTION(ile);
PHP_RSHUTDOWN_FUNCTION(ile);
PHP_MINFO_FUNCTION(ile);

PHP_FUNCTION(ile_connect);
PHP_FUNCTION(ile_callback);

ZEND_BEGIN_MODULE_GLOBALS(ile)
	long i5_allow_commit;	/* orig  - IBM i legacy CRTLIB containers fail under commit control (isolation *NONE) */
ZEND_END_MODULE_GLOBALS(ile)

#ifdef ZTS
# define MySG(v) ZEND_TSRMG(ile_globals_id, zend_ile_globals *, v)
#else
# define MySG(v) (ile_globals.v)
#endif

#endif /* PHP_ILE_H */
