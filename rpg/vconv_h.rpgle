      /if defined(VCONV_H)
      /eof
      /endif
      /define VCONV_H

       // *************************************************
       // convert API -- custom iconv convert
       // return (>-1 - good, <0 - error)
       // *************************************************
       dcl-pr convCCSID INT(10);
         fromCCSID INT(10);
         toCCSID INT(10);
         buffPtr POINTER;
         buffLen INT(10);
         outPtr POINTER;
         outLen INT(10);
       end-pr;

       dcl-pr convToILE INT(10);
         argv POINTER VALUE;
         ilev POINTER VALUE;
         ileDesc likeds(virtVar_t);
         paseDesc likeds(virtVar_t);
         ileCCSID INT(10);
         paseCCSID INT(10);
       end-pr;

       dcl-pr convToPase INT(10);
         argv POINTER VALUE;
         ilev POINTER VALUE;
         ileDesc likeds(virtVar_t);
         paseDesc likeds(virtVar_t);
         ileCCSID INT(10);
         paseCCSID INT(10);
       end-pr;

